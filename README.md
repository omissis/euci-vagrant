Drupal Ignite Provisioning
==========================

This repository contains the Ansible and Vagrant configuration necessary
to provision a fully-working instance for a Drupal (Ignite) project.


Requirements
------------

In order to setup a Drupal Ignite instance through the use of this repo, you'll need the following:

* [Git](http://git-scm.com/);
* [Vagrant](https://www.vagrantup.com/);
* [VirtualBox](https://www.virtualbox.org/).


Installation
------------

Once Git, Vagrant and VirtualBox are installed, you just have to:

1. Clone this repository in a directory of your choice (eg: `/home/your-user/workspace/your-project`);
2. Copy the `Vagrantfile.dist` to `Vagrantfile` and edit project's name and hostname;
3. Open a terminal and cd inside the cloned directory;
4. Run `vagrant up --provider=virtualbox` or `vagrant up --provider=vmware_fusion`, depending on what provider you want/can use;
5. Add the following line to your hosts file: `192.168.33.99 your-project-hostname`.

Here's a [list of known hosts file locations](http://en.wikipedia.org/wiki/Hosts_(file)) for several operating systems.

Notes
-----

* It *could* happen that the provisioning hangs the first time due to SSH timing out: if so, type `vagrant destroy` and `vagrant up --provider=virtualbox` (or `vagrant up --provider=vmware_fusion`) again.
