# Configuration for Apache 2.4.x

# This file is for DEVELOPMENT PURPOSES and therefore MUST NOT be used in production.

<VirtualHost *:80>
    ServerAdmin admin@localhost

    ServerName  {{ item.value.domain }}
    DocumentRoot {{ item.value.doc_root }}

    <Directory {{ item.value.doc_root }}>
        Order allow,deny
        Allow from all
        Options All MultiViews
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog /var/log/apache2/{{ item.value.domain }}.loc.error.log

    # Possible values include: debug, info, notice, warn, error, crit,
    # alert, emerg.
    LogLevel debug

    CustomLog /var/log/apache2/{{ item.value.domain }}.loc.access.log combined
</VirtualHost>
