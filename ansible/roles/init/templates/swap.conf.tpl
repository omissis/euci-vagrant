description "Create swap partition after vagrant mount"
version "1.0"
author "Claudio Beatrice"

start on (provisioning
      or (mounted
      or (vagrant-mounted)))
stop on runlevel [!2345]

script
    sudo /bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024
    sudo /sbin/mkswap /var/swap.1
    sudo /sbin/swapon /var/swap.1
end script
