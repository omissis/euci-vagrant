memory_limit = -1

date.timezone = {{ timezone }}

error_reporting = E_ALL | E_STRICT
display_errors = On
display_startup_errors = On
log_errors = On
log_errors_max_len = 1024
ignore_repeated_errors = Off
ignore_repeated_source = Off
report_memleaks = On
track_errors = On
html_errors = On
error_log = /var/log/apache2/php_errors.log

detect_unicode = Off
session.gc_maxlifetime = 14400
mysql.default_socket = /tmp/mysql.sock

short_open_tag = On

xdebug.max_nesting_level = 256
