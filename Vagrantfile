project_name = "euci"
project_hostname = "loc.euci.com"

Vagrant.configure("2") do |config|
    config.vm.box = "phusion/ubuntu-14.04-amd64"

    config.vm.provider :virtualbox do |v|
        v.name = project_name

        v.customize [
            "modifyvm", :id,
            "--name", project_name,
            "--memory", 512,
            "--natdnshostresolver1", "on",
            "--cpus", 1,
        ]
    end

    config.vm.provider "vmware_fusion" do |v|
        v.gui = false
        v.vmx["memsize"] = "512"
        v.vmx["numvcpus"] = "1"
    end

    config.vm.network :private_network, ip: "192.168.33.99"
    config.vm.network "forwarded_port", guest: 3306, host: 3307
    config.ssh.forward_agent = true

    config.vm.synced_folder ".", "/vagrant", :mount_options => ["dmode=755","fmode=755"]

    ###########################################################
    # Ansible provisioning (you need to have ansible installed)
    ###########################################################

    config.vm.provision "ansible" do |ansible|
        ansible.playbook = "ansible/playbook.yml"
        ansible.inventory_path = "ansible/inventories/hosts"
        ansible.limit = "all"
        ansible.extra_vars = {
            private_interface: "192.168.33.99",
            hostname: project_hostname
        }
    end
end
